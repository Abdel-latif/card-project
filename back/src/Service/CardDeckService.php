<?php

namespace App\Service;

use App\Entity\Card;

/**
 * Card deck service
 */
class CardDeckService
{
    /* List of card numbers */
    private $values = ['AS', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'Valet', 'Dame', 'Roi'];
    /* List of card colors */
    private $colors = ['Carreaux', 'Cœur', 'Pique', 'Trèfle'];

    /**
     * Returns a deck of cards using parameters
     *
     * @param bool $sort
     * @param int|null $count
     *
     * @return Card[]
     */
    public function getDeck(bool $sort, ?int $count): array
    {
        $cards = [];

        foreach ($this->values as $value) {
            foreach ($this->colors as $suit) {
                $card = new Card();
                $card->setValue($value);
                $card->setColor($suit);
                $cards[] = $card;
            }
        }

        shuffle($cards);
        $cards = $count ? array_slice($cards, 0, $count) : $cards;

        if ($sort) {
            usort($cards, [$this, 'compareCards']);
        }

        return $cards;
    }

    /**
     * Compare two cards to sort
     *
     * @param Card $card1
     * @param Card $card2
     *
     * @return int
     */
    private function compareCards(Card $card1, Card $card2): int
    {
        $suitOrder = array_flip($this->colors);
        $valueOrder = array_flip($this->values);

        if ($suitOrder[$card1->getColor()] === $suitOrder[$card2->getColor()]) {
            return $valueOrder[$card1->getValue()] <=> $valueOrder[$card2->getValue()];
        }

        return $suitOrder[$card1->getColor()] <=> $suitOrder[$card2->getColor()];
    }

    /**
     * @return float|int
     */
    public function totalCards() {
        return count($this->colors) * count($this->values);
    }
}