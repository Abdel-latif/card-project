<?php

namespace App\Controller;

use App\Service\CardDeckService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\SerializerInterface;

class ApiCardController extends AbstractController
{
    #[Route('/api/cards', name: 'api_card_deck', methods: ['GET'])]
    public function index(Request $request, CardDeckService $cardDeckService, SerializerInterface $serializer): JsonResponse
    {
        $count = json_decode($request->query->get('count', null));
        $sort = json_decode($request->query->get('sort', false));

        if ($count !== null && $count < 10 || $count > 52) {
            return new JsonResponse(['error' => 'The number of cards must be between 10 and 52.'], 400);
        }

        $deck = $cardDeckService->getDeck($sort, $count);

        $response = $serializer->serialize($deck, 'json');

        return new JsonResponse($response, 200, [
            'Access-Control-Allow-Origin' => "*",
        ], true);
    }
}
