<?php
namespace App\Tests\Service;

use App\Service\CardDeckService;
use PHPUnit\Framework\TestCase;

class CardDeckServiceTest extends TestCase
{
    public function testGetCards()
    {
        $cardService = new CardDeckService();

        // Test with 10 cards
        $result = $cardService->getDeck(false,10);
        $this->assertIsArray($result);
        $this->assertCount(10, $result);

        // Test with no count params
        $result = $cardService->getDeck(false, null);
        $this->assertIsArray($result);
        $this->assertCount($cardService->totalCards(), $result);
    }
}